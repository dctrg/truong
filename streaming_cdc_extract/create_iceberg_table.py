from pyspark.sql import *
from pyspark import SparkConf
import time as T
spark = SparkSession.builder \
  .appName("Truong_Iceberg") \
  .config("spark.sql.extensions","org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions") \
  .config("spark.jars.packages","org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.4.2") \
  .config("spark.jars.packages", "org.apache.iceberg:iceberg-spark-extensions-3.3_2.12-1.4.2") \
  .config("spark.sql.catalog.ice_catalog","org.apache.iceberg.spark.SparkCatalog") \
  .config("park.sql.catalog.ice_catalog.type","hive") \
  .config("spark.sql.catalog.ice_catalog.uri","thrift://10.100.21.133:9083") \
  .config("spark.sql.warehouse.dir","hdfs://hdfsha/warehouse/tablespace/managed/hive") \
  .enableHiveSupport() \
  .getOrCreate()

#################### Create database
spark.sql("create database if not exists extract_crypto")

#################### Create table
spark.sql("""
    CREATE TABLE IF NOT EXISTS ice_catalog.extract_crypto.crypto_currency (
        id int,
        symbol string,
        open_timestamp timestamp,
        open_price decimal(18,2),
        high_price decimal(18,2),
        low_price decimal(18,2),
        close_price decimal(18,2),
        volume decimal(18,2),
        taker_buy_quote_asset_volume decimal(18,9),
        taker_buy_base_asset_volume decimal(18,9),
        quote_asset_volume decimal(18,9),
        number_of_trades int,
        op string,
        load_date TIMESTAMP ,
        last_modified_date TIMESTAMP
    )
    USING iceberg
    TBLPROPERTIES (
    'write.format.default'='orc',
    'write.distribution-mode'='hash',
    'format-version'='2',
    'write.merge.mode'='copy-on-write',
    'write.update.mode'='copy-on-write',
    'write.delete.mode'='copy-on-write',
    'write.metadata.delete-after-commit.enabled'='true',
    'write.metadata.previous-versions-max'='10')
""")

spark.sql("select * from ice_catalog.extract_crypto.crypto_currency").show(10, False)