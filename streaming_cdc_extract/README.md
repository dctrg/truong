# streaming cdc debezium

## Structure repo

```text
.
|-- README.md
|-- job
|   `-- spark_streaming_job.py
|-- job_entries
|   `-- job_stream.json
|-- libs
|   |-- __init__.py
|   `-- cdc_process_util.py
`-- requirements.txt

```

## Setting up

### Require:

- spark: 3.5.0
- pyspark: 3.5.0
- python >=3.8

### Install

- `pip install -r requirements.txt`

### Entry config file:

```json
{
  "database_name": "",
  "table_name": "",
  "schema_type": [],
  "port_code": "",
  "keys": [],
  "kafka_config": {
    "kafkaserver": "172.16.101.17:9092,172.16.101.18:9092,172.16.101.19:9092",
    "topic": "",
    "startingoffsets": "latest"
  }
}
```

- Naming for config file: `job_stream_{database}_{table}.json`

### Run streaming job

```text
spark-submit \
--config job_entries/{filename}\
--jobname {jobname for run spark application}\
--checkpointpath /home/hadoop/checkpoint \
```
