import sys
import time
import datetime as dt
from pyspark.sql.functions import col, from_json, schema_of_json, lit, row_number, from_unixtime, to_timestamp, trim, coalesce
from pyspark.sql import DataFrame, Column
from pyspark.sql.types import *
from pyspark.sql.window import Window
import json
from pyspark.sql.functions import *
import random


def getShowString(df, n=10, truncate=True, vertical=False):
    if isinstance(truncate, bool) and truncate:
        return df._jdf.showString(n, 10, vertical)
    else:
        return df._jdf.showString(n, int(truncate), vertical)


class CDCProcessUtil:
    def __init__(self,
                 spark,
                 tableconffile,
                 jobname):
        self.spark = spark
        self.tableconffile = tableconffile
        self.jobname = jobname

        self.tables_config = self._load_tables_config(tableconffile)
   
        self.record_date = date_format(lit(dt.datetime.now()),"yyyy-MM-dd HH:mm:ss")
        if self.tables_config.get("port_code"):
            self.port_code = self.tables_config.get("port_code")
        else:
            self.port_code = None
 
        if self.tables_config.get("rename_dict"):
            self.rename_dict = self.tables_config.get("rename_dict")
        else:
            self.rename_dict = None
            
    def _load_tables_config(self, config_path):
        json_content = json.load(open(config_path))
        return json_content
    
    def _IsInteger(self, variable):
        return isinstance(variable, int)
    
    def processBatch(self, data_frame_batch, batchId):
        if (data_frame_batch.count() > 0):
            data_frame = data_frame_batch.cache()

            schema_level_one = StructType([
                StructField("schema", StringType(), True),
                StructField("payload", StringType(), True)
            ])

            dataJsonDF = data_frame.select(
                from_json(col("value").cast("string"), schema_level_one).alias("data")).select(
                col("data.*"))

            schema_level_two = StructType([
                StructField("before", StringType(), True),
                StructField("after", StringType(), True),
                StructField("source", StringType(), True),
                StructField("op", StringType(), True),
                StructField("ts_ms", LongType(), True)
            ])

            dataJsonDF2 = dataJsonDF.select(
                from_json(col("payload").cast("string"), schema_level_two).alias("data")).select(
                col("data.*"))
            print(dataJsonDF2.show(20, False))
            dataInsert = dataJsonDF2.filter("op in ('r','c') and after is not null")

            dataUpsert = dataJsonDF2.filter("op in ('u') and after is not null")

            dataDelete = dataJsonDF2.filter("op in ('d') and before is not null")

            if dataInsert.count() > 0:
             
                sourceJson = dataInsert.select('source').first()
                schemaSource = schema_of_json(sourceJson[0])

                datatables = dataInsert.select(
                    from_json(col("source").cast("string"),
                              schemaSource).alias("SOURCE")) \
                    .select(col("SOURCE.db"), col("SOURCE.table")).distinct()

                rowtables = datatables.collect()

                for cols in rowtables:
                    tableName = cols[1]
                    dataDF = dataInsert.select(col("after"),
                                               col("op"),
                                               col("ts_ms"),
                                               from_json(col("source").cast("string"),
                                                         schemaSource).alias("SOURCE")) \
                        .filter("SOURCE.table = '" + tableName + "'")
                    datajson = dataDF.select('after', 'op').first()
                    schema_struct = []
                    #for schema in self.tables_config.get("schema_type"):
                    #    schema_struct.append(
                    #        StructField(schema.get("column_name"), globals()[schema.get("type")](*schema.get("params",[])), True))
                    for schema in self.tables_config.get("schema_type"):
                        data_type_params = schema.get("params", [])
                        if data_type_params:
                            element_type_name = schema["params"][0]
                            if self._IsInteger(element_type_name):
                                data_type_instance =  globals()[schema.get("type")](*schema.get("params",[]))
                            else:
                                element_type_class = globals()[element_type_name]
                                element_type_instance = element_type_class()
                                data_type_instance =  globals()[schema["type"]](element_type_instance)
                        else:
                            data_type_instance = globals()[schema["type"]]()
                        schema_struct.append(
                            StructField(schema.get("column_name"), data_type_instance, True))

                    schemadata = StructType(schema_struct)
                   

                    dataDFOutput = dataDF.select(col("op"),
                                                 from_json(col("after").cast("string"), schemadata).alias(
                                                     "DFADD")).select(col("DFADD.*"),
                                                                      col("op"))
                    self._InsertExtract(dataDFOutput)
            if dataUpsert.count() > 0:
                sourcejson = dataUpsert.select('source').first()
                schemasource = schema_of_json(sourcejson[0])
                datatables = dataUpsert.select(from_json(col("source").cast("string"), schemasource).alias("SOURCE")) \
                    .select(col("SOURCE.db"), col("SOURCE.table")).distinct()

                rowtables = datatables.collect()
                for cols in rowtables:
                    tableName = cols[1]
                    dataDF = dataUpsert.select(
                        col("after"),
                        col("op"),
                        col("ts_ms"),
                        from_json(col("source").cast("string"), schemasource).alias("SOURCE")) \
                        .filter("SOURCE.table = '" + tableName + "'")
                    datajson = dataDF.select('after', 'op', 'ts_ms').first()
                    schema_struct = []
                    #for schema in self.tables_config.get("schema_type"):
                     #   schema_struct.append(
                      #      StructField(schema.get("column_name"), globals()[schema.get("type")](*schema.get("params",[])), True))
                    for schema in self.tables_config.get("schema_type"):
                        data_type_params = schema.get("params", [])
                        if data_type_params:
                            element_type_name = schema["params"][0]
                            if self.is_integer(element_type_name):
                                data_type_instance =  globals()[schema.get("type")](*schema.get("params",[]))
                            else:
                                element_type_class = globals()[element_type_name]
                                element_type_instance = element_type_class()
                                data_type_instance =  globals()[schema["type"]](element_type_instance)
                        else:
                            data_type_instance = globals()[schema["type"]]()
                        schema_struct.append(
                            StructField(schema.get("column_name"), data_type_instance, True))
                    schemadata = StructType(schema_struct)
                  

                    dataDFOutput = dataDF.select(col("op"),
                                                 col("ts_ms"), from_json(col("after").cast("string"), schemadata).alias(
                            "DFADD")).select(col("DFADD.*"), col("op"), col("ts_ms"))
                    keys = self.tables_config.get("keys")
                    w = Window.partitionBy(*keys).orderBy(dataDFOutput["ts_ms"].desc())
                    dataDFOutput = dataDFOutput.withColumn("rownum", row_number().over(w)) \
                        .filter("rownum = 1") \
                        .drop("rownum", "ts_ms")
                    self._MergeIntoDataLake(dataDFOutput, batchId)

            if (dataDelete.count() > 0):
               
                sourceJson = dataDelete.select('source').first()
                schemaSource = schema_of_json(sourceJson[0])

                datatables = dataDelete.select(
                    from_json(col("source").cast("string"),
                              schemaSource).alias("SOURCE")) \
                    .select(col("SOURCE.db"), col("SOURCE.table")).distinct()

                rowtables = datatables.collect()

                for cols in rowtables:
                    tableName = cols[1]
                    dataDF = dataDelete.select(col("before"),
                                               col("op"),
                                               col("ts_ms"),
                                               from_json(col("source").cast("string"),
                                                         schemaSource).alias("SOURCE")) \
                        .filter("SOURCE.table = '" + tableName + "'")
                    
                    schema_struct = []
                    #for schema in self.tables_config.get("schema_type"):
                    #    schema_struct.append(
                    #        StructField(schema.get("column_name"), globals()[schema.get("type")](*schema.get("params",[])), True))
                    
                    for schema in self.tables_config.get("schema_type"):
                        data_type_params = schema.get("params", [])
                        if data_type_params:
                            element_type_name = schema["params"][0]
                            if self.is_integer(element_type_name):
                                data_type_instance =  globals()[schema.get("type")](*schema.get("params",[]))
                            else:
                                element_type_class = globals()[element_type_name]
                                element_type_instance = element_type_class()
                                data_type_instance =  globals()[schema["type"]](element_type_instance)
                        else:
                            data_type_instance = globals()[schema["type"]]()
                        schema_struct.append(
                            StructField(schema.get("column_name"), data_type_instance, True))

                    schemadata = StructType(schema_struct)

                    dataDFOutput = dataDF.select(col("op"),
                                                 from_json(col("before").cast("string"), schemadata).alias(
                                                     "DFDEL")).select(col("DFDEL.*"),
                                                                      col("op"))

                    self._DeleteDataLake(dataDFOutput, batchId)
                    
    def _preprocess_col(self, df, col_name, default_value=None):
        ### Preprocess string
        col_dtype = dict(df.dtypes)[col_name]
        if col_dtype == "string":
            col_instance = trim(col(col_name))
        else:
            col_instance = col(col_name)
            
        ### Preprocess null
        if default_value is not None:
            default_value = lit(default_value)
            if col_dtype == "timestamp":
                default_value = default_value.cast("timestamp")
            col_instance = coalesce(col_instance, default_value)
        
        return col_instance

    def _preprocess_dataframe(self, df):
        for c_name in df.columns:
            for schema in self.tables_config.get("schema_type"):
                if schema.get("column_name") == c_name:
                    default_value = schema.get("default_value")
            df = df.withColumn(c_name, self._preprocess_col(df, c_name, default_value))
        return df
    
    ## Rename column 
    def _rename_column(self,df, rename_dict = {}):
        if rename_dict != {}:
            for old_name, new_name in rename_dict.items():
                df = df.withColumnRenamed(old_name, new_name)
        return df
    
    ## Drop column (source uat vs extract)
    
    def _DropColumn(self, df, drop_column = []):
        if drop_column != []:
            df = df.drop(*drop_column)
        return df

    def _InsertExtract(self, df):
        database_name = self.tables_config["database_name"]
        table_name = self.tables_config["table_name"]

        df = self._preprocess_dataframe(df)
        if self.rename_dict:
            df = self._rename_column(df,self.rename_dict)
        df = df.withColumn("load_date", to_timestamp(lit(self.record_date)))
        df = df.withColumn("last_modified_date", to_timestamp(lit(self.record_date)))

        if self.port_code:
            df = df.withColumn("port_code", lit(self.port_code))

        df.write.format("iceberg").mode("append").save(f"{database_name}.{table_name}")
 
    def _MergeIntoDataLake(self, dataFrame, batchId):
        dataFrame = self._preprocess_dataframe(dataFrame)
        if self.rename_dict:
            dataFrame = self._rename_column(dataFrame,self.rename_dict)
        if self.port_code:
            dataFrame = dataFrame.withColumn("port_code", lit(self.port_code))
        database_name = self.tables_config["database_name"]
        table_name = self.tables_config["table_name"]
        keys = self.tables_config["keys"]
  
        t = random.randint(3, 10000000000)
        TempTable = "tmp_update_" + table_name + "_u_" + str(batchId) + "_" + str(t)
        dataFrame = dataFrame.withColumn("last_modified_date",to_timestamp(lit(self.record_date)))
        dataFrame.createOrReplaceGlobalTempView(f"{TempTable}")
      
        conditions = []
        for key in keys:
            condition = f"t.{key} = u.{key}"
            conditions.append(condition)
        full_condition = " and ".join(conditions)

        update_commands = []
        for column in dataFrame.columns:
            update_command = f"{column} = u.{column}"
            update_commands.append(update_command)
       
        full_update_commands = " , ".join(update_commands)

        query = f"""MERGE INTO {database_name}.{table_name} t USING (SELECT * FROM global_temp.{TempTable}) u
                ON {full_condition}
                WHEN MATCHED THEN UPDATE SET {full_update_commands} """

        print("####### Execute SQL:" + query)
        try:
            self.spark.sql(query)
        except Exception as err:
            print("Error of MERGE INTO")
            print(err)
            pass
        self.spark.catalog.dropGlobalTempView(TempTable)

    def _DeleteDataLake(self, dataFrame, batchId):
        dataFrame = self._preprocess_dataframe(dataFrame)
        if self.rename_dict:
            dataFrame = self._rename_column(dataFrame,self.rename_dict)
        if self.port_code:
            dataFrame = dataFrame.withColumn("port_code", lit(self.port_code))
        database_name = self.tables_config["database_name"]
        table_name = self.tables_config["table_name"]
        keys = self.tables_config["keys"]
    
        t = random.randint(3, 10000000000)
        TempTable = "tmp_delete_" + table_name + "_u_" + str(batchId) + "_" + str(t)
        dataFrame = dataFrame.withColumn("last_modified_date",to_timestamp(lit(self.record_date)))
        dataFrame.createOrReplaceGlobalTempView(TempTable)
     
        conditions = []
        for key in keys:
            condition = f"t.{key} = u.{key}"
            conditions.append(condition)
        full_condition = " and ".join(conditions)
        delete_commands = []
        for column in dataFrame.columns:
            delete_command = f"{column} = u.{column}"
            delete_commands.append(delete_command)
      
        full_delete_commands = " , ".join(delete_commands)

        query = f"""MERGE INTO {database_name}.{table_name} t USING (SELECT * FROM global_temp.{TempTable}) u
                ON {full_condition}
                WHEN MATCHED THEN UPDATE SET {full_delete_commands} """

        print("####### Execute SQL:" + query)
        try:
            self.spark.sql(query)
        except Exception as err:
            print("Error of MERGE INTO")
            print(err)
            pass
        self.spark.catalog.dropGlobalTempView(TempTable)
