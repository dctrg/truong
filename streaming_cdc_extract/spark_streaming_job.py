import argparse
import json
import sys
import os
import time as T

from pyspark.sql import SparkSession
# sys.path.append('../libs')
from libs.cdc_process_util import CDCProcessUtil

parser = argparse.ArgumentParser()
parser.add_argument("-cf", "--configuration", type=str, required=True)
parser.add_argument("-j", "--jobname", type=str, required=True)
parser.add_argument("-ch", "--checkpointpath", type=str, required=True)
#parser.add_argument("-w", "--warehousepath", type=str, required=True)
#parser.add_argument("-fs", "--defaultFS", type=str, required=True)

args = parser.parse_args()

#WAREHOUSE_LOCATION = args.warehousepath
#DEFAULT_FS = args.defaultFS
JOB_NAME = args.jobname
CHECKPOINT_LOCATION = args.checkpointpath
config = json.load(open(args.configuration))


spark = SparkSession.builder \
    .appName(JOB_NAME) \
    .config("spark.scheduler.mode", "FAIR") \
    .config("spark.sql.legacy.timeParserPolicy","LEGACY") \
    .config("spark.sql.extensions","org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions") \
    .config("spark.jars.packages","org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.4.2") \
    .config("spark.jars.packages", "org.apache.iceberg:iceberg-spark-extensions-3.3_2.12-1.4.2") \
    .config("spark.sql.catalog.ice_catalog","org.apache.iceberg.spark.SparkCatalog") \
    .config("spark.sql.catalog.ice_catalog.type","hive") \
    .config("spark.sql.catalog.ice_catalog.uri","thrift://10.100.21.133:9083") \
    .config("spark.sql.warehouse.dir","hdfs://hdfsha/warehouse/tablespace/managed/hive") \
    .enableHiveSupport() \
    .getOrCreate()

sc = spark.sparkContext
log4j = sc._jvm.org.apache.log4j
logger = log4j.LogManager.getLogger(__name__)


checkpointpath = CHECKPOINT_LOCATION + "/" + JOB_NAME + "/checkpoint/"
kafka_options = {
    "kafka.bootstrap.servers": config["kafka_config"]["kafkaserver"],
    "subscribe": config["kafka_config"]["topic"],
    "kafka.consumer.commit.groupid": "group-" + JOB_NAME,
    "inferSchema": "true",
    "classification": "json",
    "failOnDataLoss": "false",
    "maxOffsetsPerTrigger": 10000,
    "max.partition.fetch.bytes": 10485760,
    "startingOffsets": config["kafka_config"]["startingoffsets"],
}

reader = spark \
    .readStream \
    .format("kafka") \
    .options(**kafka_options)
reader.option("startingOffsets", config["kafka_config"]["startingoffsets"])

kafka_data = reader.load()

source_data = kafka_data.selectExpr("CAST(value AS STRING)")

process = CDCProcessUtil(spark, args.configuration, JOB_NAME)
source_data \
    .writeStream \
    .outputMode("append") \
    .trigger(processingTime="20 seconds") \
    .foreachBatch(process.processBatch) \
    .option("checkpointLocation", checkpointpath) \
    .start() \
    .awaitTermination()