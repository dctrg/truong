from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField, IntegerType, DoubleType, LongType

spark = SparkSession.builder.appName('Enrich').getOrCreate()

schema = StructType([
    StructField("OPEN_TIMESTAMP", LongType(), True),   # Unix timestamp
    StructField("OPEN_PRICE", DoubleType(), True),           # Price in quote asset (USDT here)
    StructField("HIGH_PRICE", DoubleType(), True),           # Highest price
    StructField("LOW_PRICE", DoubleType(), True),            # Lowest price
    StructField("CLOSE_PRICE", DoubleType(), True),          # Closing price
    StructField("VOLUME", DoubleType(), True),         # Volume traded, in base asset (BTC)
    StructField("TAKER_BUY_QUOTE_ASSET_VOLUME", DoubleType(), True), # Taker buy quote asset volume
    StructField("TAKER_BUY_BASE_ASSET_VOLUME", DoubleType(), True),  # Taker buy base asset volume
    StructField("QUOTE_ASSET_VOLUME", DoubleType(), True),           # Volume traded, in quote asset (USDT)
    StructField("NUMBER_OF_TRADES", IntegerType(), True)             # Number of trades
])

def read_data(symbol):
    df = spark.read.format('csv')\
              .options(header=False, delimiter = '|') \
              .schema(schema) \
              .load(f"/truong/da_data/{symbol}.csv")
    return df

def transform_data(df,symbol):
    df = df.withColumn('OPEN_TIMESTAMP', to_timestamp(from_unixtime(df.OPEN_TIMESTAMP + 25200, "yyyy-MM-dd HH:mm:ss")))
    df = df.withColumn('SYMBOL', lit(f'{symbol}'))
    df = df.select('SYMBOL', 'OPEN_TIMESTAMP', 'OPEN_PRICE', 'HIGH_PRICE', 'LOW_PRICE', 'CLOSE_PRICE', 'VOLUME', 'TAKER_BUY_QUOTE_ASSET_VOLUME', 'TAKER_BUY_BASE_ASSET_VOLUME', 'QUOTE_ASSET_VOLUME', 'NUMBER_OF_TRADES')
    return df

def write_data(df):
    df.write.format('jdbc').options(
        url='jdbc:mysql://10.100.21.122:3306/truong',
        driver='com.mysql.cj.jdbc.Driver',
        dbtable='CRYPTO_CURRENCY',
        user='test',
        password='Natis_4U'
    ).mode('append').save()

if __name__ == '__main__':
    symbol_list = ['BTCUSDT','ETHUSDT','BNBUSDT']
    for symbol in symbol_list:
        df = read_data(symbol)
        df = transform_data(df,symbol)
        write_data(df)
        print(f"Write {symbol} into database successfully!")
