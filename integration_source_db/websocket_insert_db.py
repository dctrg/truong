import json
import pandas as pd
import websocket
import configparser
import time
import mysql.connector
config = configparser.ConfigParser()
config.read_file(open('/home/hadoop/truong/da_code/truong/config.cfg')) # sua path
DB_HOST = config.get('MY_SQL', 'DB_HOST')
DB_USER = config.get('MY_SQL', 'DB_USER')
DB_PASSWORD = config.get('MY_SQL', 'DB_PASSWORD')
DB_NAME = config.get('MY_SQL', 'DB_NAME')
def connect_db():
    connect_params = {
        "user" : DB_USER,
        "password" : DB_PASSWORD,
        "host" : DB_HOST,
        "database" : DB_NAME,
    }
    conn = mysql.connector.connect(**connect_params)
    cur = conn.cursor()
    return conn, cur

def manipulation(message):
    candle = message['data']['k']
    symbol = message['data']['s']
    candle['t'] = int(candle['t']) + 25200000 # GMT+7
    open_time = pd.to_datetime(candle['t'], unit='ms')
    close = float(candle['c'])
    open = float(candle['o'])
    high = float(candle['h'])
    low = float(candle['l'])
    volume = float(candle['v'])
    taker_quota = float(candle['Q'])
    taker_base = float(candle['V'])
    quota_volume = float(candle['q'])
    number_of_trades = int(candle['n'])
    
    conn, cur = connect_db()
    
    # Insert the data into the table
    query = """
            INSERT INTO truong.CRYPTO_CURRENCY
            (SYMBOL, OPEN_TIMESTAMP, OPEN_PRICE, HIGH_PRICE, LOW_PRICE, CLOSE_PRICE, VOLUME, TAKER_BUY_QUOTE_ASSET_VOLUME, TAKER_BUY_BASE_ASSET_VOLUME, QUOTE_ASSET_VOLUME, NUMBER_OF_TRADES) 
            VALUES 
            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """
    values = (symbol, open_time, open, high, low, close, volume, taker_quota, taker_base, quota_volume, number_of_trades)

    try:
        cur.execute(query, values)
        conn.commit()
    except Exception as e:
        print(e)
    
    # Commit the changes and close the connection
    cur.close()
    conn.close()
        
def on_open(ws):
    print("Connection opened")


# def on_close(ws,close_status_code, close_msg):
#     print("Closed connection")
#     if close_status_code or close_msg:
#         print("close status code: " + str(close_status_code))
#         print("close message: " + str(close_msg))


def on_error(ws, error):
    print(error)

def on_message(ws,message):
  message = json.loads(message)
  print(message)
  manipulation(message)

def run_websocket():
    ws = websocket.WebSocketApp(socket, on_open=on_open, on_close=on_close, on_error=on_error, on_message=on_message)
    ws.run_forever()
    
def attempt_reconnect(initial_delay=1, max_attempts=5):
    delay = initial_delay
    attempt = 0

    while attempt < max_attempts:
        try:
            print(f"Attempting to reconnect (Attempt {attempt + 1}/{max_attempts})...")
            run_websocket()
            break  # If connection is successful, break out of the loop
        except Exception as e:
            print(f"Reconnection failed: {e}")
            attempt += 1
            time.sleep(delay)
            delay *= 2  # Exponential backoff

    if attempt == max_attempts:
        print("Failed to reconnect after several attempts.")

# Modify the on_close function
def on_close(ws,close_status_code, close_msg):
    print("Closed connection. Attempting to reconnect...")
    attempt_reconnect()
    print("Closed connection")
    if close_status_code or close_msg:
        print("close status code: " + str(close_status_code))
        print("close message: " + str(close_msg))


if __name__ == '__main__':
    assets = ['BTCUSDT','ETHUSDT','BNBUSDT']
    assets = [coin.lower() + '@kline_1s' for coin in assets]
    assets = '/'.join(assets)

    socket = "wss://stream.binance.com:9443/stream?streams="+assets

    ws = websocket.WebSocketApp(socket, on_open=on_open, on_close=on_close, on_error=on_error, on_message=on_message)
    ws.run_forever()
