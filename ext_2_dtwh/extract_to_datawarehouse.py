import os
import argparse
import json

from pyspark.sql import SparkSession
import datetime as dt
from pyspark import SparkConf
import pyspark.sql.functions as f
from pyspark.sql.functions import *
from pyspark.sql.types import *

from libs.Transform import transform_symbol_df, transform_date_df, transform_time_df
from libs.Extract_iceberg import Current
from libs.Load import write_data
from libs.Extract_db import get_data_from_source

### Parameter
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", type=str, required=True)
parser.add_argument("-j", "--jobname", type=str, required=True)
parser.add_argument("-s", "--startdate", type=str, required=False)
parser.add_argument("-e", "--enddate", type=str, required=False)
parser.add_argument("-b", "--backfilldate", type=str, required=False)
#parser.add_argument("-w", "--warehousepath", type=str, required=True)
args = parser.parse_args()

#WAREHOUSE_LOCATION = args.warehousepath
JOB_NAME = args.jobname
START_DATE = args.startdate
END_DATE = args.enddate
BACKFILL_DATE = args.backfilldate
config = json.load(open(args.config))

## khoi tao spark session
spark = SparkSession.builder \
    .appName(JOB_NAME) \
    .config("spark.scheduler.mode", "FAIR") \
    .config("spark.sql.legacy.timeParserPolicy","LEGACY") \
    .config("spark.sql.extensions","org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions") \
    .config("spark.jars.packages","org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.4.2") \
    .config("spark.jars.packages", "org.apache.iceberg:iceberg-spark-extensions-3.3_2.12-1.4.2") \
    .config("spark.sql.catalog.ice_catalog","org.apache.iceberg.spark.SparkCatalog") \
    .config("spark.sql.catalog.ice_catalog.type","hive") \
    .config("spark.sql.catalog.ice_catalog.uri","thrift://10.100.21.133:9083") \
    .config("spark.sql.warehouse.dir","hdfs://hdfsha/warehouse/tablespace/managed/hive") \
    .enableHiveSupport() \
    .getOrCreate()

sc = spark.sparkContext
log4j = sc._jvm.org.apache.log4j
logger = log4j.LogManager.getLogger(__name__)

current = Current(spark,config["database_name"],config["table_name_current"])

###### get data from current
print("===================================")
print(START_DATE)
print(END_DATE)
print(BACKFILL_DATE)
print("====================================")
if START_DATE==None and END_DATE ==None:
    df_current = current.extract_by_date(config["date_field"],BACKFILL_DATE)
elif START_DATE !=None and END_DATE !=None:
    df_current = current.extract_by_date_range(config["date_field"],START_DATE,END_DATE)
else:
    print("Input not valid")
    spark.stop()

###### transform data into dim dataframe
df_symbol = transform_symbol_df(spark,df_current)
df_date = transform_date_df(df_current)
df_time = transform_time_df(spark,df_current)

###### write data into dim table
write_data(df_symbol,config["symbol_table"])
write_data(df_date,config["date_table"])
write_data(df_time,config["time_table"])

###### create dataframe from dim table
table_name = "dim_symbol"
rows_symbol, col_names_symbol = get_data_from_source(table_name)
df_symbol_dim = spark.createDataFrame(rows_symbol, col_names_symbol)

table_name = "dim_date"
rows_date, col_names_date = get_data_from_source(table_name)
df_date_dim = spark.createDataFrame(rows_date, col_names_date)

table_name = "dim_time"
rows_time, col_names_time = get_data_from_source(table_name)
df_time_dim = spark.createDataFrame(rows_time, col_names_time)

###### join table
df_fact = df_current.select(['symbol', 'open_timestamp', 'open_price', 'high_price', 'low_price', 'close_price', 'volume', 'taker_buy_quote_asset_volume', 'taker_buy_base_asset_volume', 'quote_asset_volume', 'number_of_trades'])
df_fact = df_fact.withColumn("date_col", to_date(df_fact.open_timestamp))
df_fact = df_fact.withColumn("time_col", split(df_fact.open_timestamp,' ').getItem(1))

df_fact = df_fact.join(df_symbol_dim, df_fact['symbol'] == df_symbol_dim['symbol'], 'inner')\
    .join(df_date_dim, df_fact['date_col'] == df_date_dim['date_col'], 'inner')\
    .join(df_time_dim, df_fact['time_col'] == df_time_dim['time_col'], 'inner')

df_fact = df_fact.select(['symbol_skey', 'date_skey', 'time_skey', 'open_price', 'high_price', 'low_price', 'close_price', 'volume', 'taker_buy_quote_asset_volume', 'taker_buy_base_asset_volume', 'quote_asset_volume', 'number_of_trades'])
df_fact = df_fact.withColumnRenamed("symbol_skey","symbol_key")\
                .withColumnRenamed("date_skey","date_key")\
                .withColumnRenamed("time_skey","time_key")

###### write data into fact table
write_data(df_fact,config["fact_table"])