from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField
from libs.Extract_db import get_data_from_source

def transform_symbol_df(spark,df):
    df_symbol = df.select("symbol").distinct()
    df_symbol = df_symbol.withColumn("quote_asset", split(df_symbol['symbol'], "USDT").getItem(0))
    df_symbol = df_symbol.withColumn("base_asset", lit('USDT'))
    table_name = "dim_symbol"
    rows, col_names = get_data_from_source(table_name)
    if len(rows) != 0:
        df_symbol_dim = spark.createDataFrame(rows, col_names)
        df_symbol = df_symbol.join(df_symbol_dim, df_symbol['symbol'] == df_symbol_dim['symbol'], 'leftanti')
    else:
        print("No data in dim_symbol")
    return df_symbol

def transform_date_df(df):
    df_date = df.select("open_timestamp")
    df_date = df_date.withColumn("date_col", to_date(df.open_timestamp))
    df_date = df_date.select("date_col").distinct()
    df_date = df_date.withColumn("year", year(df_date.date_col))
    df_date = df_date.withColumn("month", month(df_date.date_col))
    df_date = df_date.withColumn("day", dayofmonth(df_date.date_col))
    df_date = df_date.withColumn("quarter", quarter(df_date.date_col))
    df_date = df_date.withColumn("week", weekofyear(df_date.date_col))
    df_date = df_date.withColumn("weekday", dayofweek(df_date.date_col))
    return df_date

def transform_time_df(spark,df):
    df_time = df.select("open_timestamp")
    df_time = df_time.withColumn("time_col", split(df.open_timestamp,' ').getItem(1))
    df_time = df_time.select("time_col").distinct()
    df_time = df_time.withColumn("hour", hour(df_time.time_col))
    df_time = df_time.withColumn("minute", minute(df_time.time_col))
    df_time = df_time.withColumn("second", second(df_time.time_col))
    table_name = "dim_time"
    rows, col_names = get_data_from_source(table_name)
    if len(rows) != 0:
        df_tim_dim = spark.createDataFrame(rows, col_names)
        df_time = df_time.join(df_tim_dim, df_time['time_col'] == df_tim_dim['time_col'], 'leftanti')
    else:
        print("No data in dim_time")
    return df_time