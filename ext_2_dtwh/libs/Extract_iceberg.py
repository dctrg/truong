import datetime as dt
class Current:
    def __init__(self, spark, database, table):
        self.spark = spark
        self.database = database
        self.table = table

    def extract_by_date(self, date_field, backfill_date=None):
        if backfill_date:
            extract_date = backfill_date
        else:
            extract_date = dt.date.today()
        print("extract_date", extract_date)
        df_extract = self.spark.sql(f"SELECT * FROM {self.database}.{self.table} WHERE CAST({date_field} AS DATE) = '{extract_date}'")
        return df_extract

    def extract_by_date_range(self, date_field, start_date, end_date):
        df_extract = self.spark.sql(
            f"SELECT * FROM {self.database}.{self.table} WHERE CAST({date_field} AS DATE) >= '{start_date}' AND CAST({date_field} AS DATE) <= '{end_date}'")
        return df_extract
