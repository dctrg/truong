import oracledb
import configparser

config = configparser.ConfigParser()
config.read_file(open('/home/hadoop/truong/da_code/truong/config.cfg')) # sua path
DB_DSN = config.get('ORACLE', 'DB_DSN')
DB_USER = config.get('ORACLE', 'DB_USER')
DB_PASSWORD = config.get('ORACLE', 'DB_PASSWORD')

def connect_db():
    connect_params = {
        "user" : DB_USER,
        "password" : DB_PASSWORD,
        "dsn" : DB_DSN,
    }
    conn = oracledb.connect(**connect_params)
    cur = conn.cursor()
    return conn, cur

def get_data_from_source(table_name):
    conn, cur = connect_db()
    cur.execute(f"SELECT * FROM truong.{table_name}")
    col_names = [row[0] for row in cur.description]
    rows = cur.fetchall()
    return rows, col_names